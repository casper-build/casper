using System;
using System.Linq;
using Casper;

foreach (var projectType in typeof(Projects).Assembly.GetTypes().Where(t => t.Name == "Project"))
{
	Console.WriteLine(projectType);
}

var build = new Exec {
	TargetFilePath = "dotnet",
	Arguments = "build -v detailed",
};

var test = new Exec {
	TargetFilePath = "dotnet",
	Arguments = "test",
};

// build.Run();
// test.Run();

Projects.Root.IO_Testing.Compile.Run();
Projects.Root.Core.Compile.Run();
