using NuGet.Frameworks;

namespace Casper;

[Timeout(3000)]
public class PackageReferenceTests {
	// These tests assume the target package has already been restored
	// These tests are hardly exhaustive, but as we are relying on NuGet, they don't need to be

	[Test]
	public void NUnit() {
		var packageReference = new PackageReference("NUnit", "3.13.3");

		Assert.Multiple(() => {
			ShouldChooseFor(packageReference, "net45", "net48");
			ShouldChooseFor(packageReference, "net45", "net462");
			ShouldChooseFor(packageReference, "net45", "net451");
			ShouldChooseFor(packageReference, "net45", "net45");
			ShouldChooseFor(packageReference, "net40", "net403");
			ShouldChooseFor(packageReference, "net40", "net40");
			ShouldChooseFor(packageReference, "net35", "net35");
			ShouldChooseFor(packageReference, null, "net20");
			ShouldChooseFor(packageReference, null, "net11");
			ShouldChooseFor(packageReference, "netstandard2.0", "netstandard2.1");
			ShouldChooseFor(packageReference, "netstandard2.0", "netstandard2.0");
			ShouldChooseFor(packageReference, null, "netstandard1.3");
			ShouldChooseFor(packageReference, null, "netstandard1.0");
		});
	}

	[Test]
	public void Newtonsoft() {
		var packageReference = new PackageReference("Newtonsoft.Json", "13.0.2");

		Assert.Multiple(() => {
			ShouldChooseFor(packageReference, "net6.0", "net6.0");
			ShouldChooseFor(packageReference, "net45", "net48");
			ShouldChooseFor(packageReference, "net45", "net462");
			ShouldChooseFor(packageReference, "net45", "net451");
			ShouldChooseFor(packageReference, "net45", "net45");
			ShouldChooseFor(packageReference, "net40", "net403");
			ShouldChooseFor(packageReference, "net40", "net40");
			ShouldChooseFor(packageReference, "net35", "net35");
			ShouldChooseFor(packageReference, "net20", "net20");
			ShouldChooseFor(packageReference, null, "net11");
			ShouldChooseFor(packageReference, "netstandard2.0", "netstandard2.1");
			ShouldChooseFor(packageReference, "netstandard2.0", "netstandard2.0");
			ShouldChooseFor(packageReference, "netstandard1.3", "netstandard1.3");
			ShouldChooseFor(packageReference, "netstandard1.0", "netstandard1.0");
		});
	}

	private void ShouldChooseFor(PackageReference packageReference, string? net45, string net462) {
		var chosenFramework = packageReference.ChooseFramework(NuGetFramework.Parse(net462));

		Assert.That(chosenFramework?.Name, Is.EqualTo(net45));
	}
}
