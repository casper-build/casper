using Casper.IO;

namespace Casper;

[TestFixture]
public class CompileCSharpTests {
	readonly IFileSystem fileSystem = RealFileSystem.Instance;
	readonly IDirectory workingDirectory;

	public CompileCSharpTests() {
		workingDirectory = fileSystem.MakeTemporaryDirectory();
	}

	public void Dispose() {
		workingDirectory.Delete();
	}

	[Test]
	[Timeout(3000)]
	public void Compile() {
		var classA = workingDirectory.File("ClassA.cs");
		var classB = workingDirectory.File("ClassB.cs");
		var assemblyFile = workingDirectory.File("Compile.dll");

		classA.WriteAllText(@"
class ClassA { public static void A() { ClassB.B(); } }
		                    ");
		classB.WriteAllText(@"
class ClassB { public static void B() { ClassA.A(); } }
		                    ");
		assemblyFile.Delete();

		var target = new CompileCSharp {
			Input = {
				SourceFiles = { classA, classB },
				TargetFramework = TargetFrameworks.Net60,
			},
			Output = {
				AssemblyName = "Compile",
				Directory = workingDirectory.FullPath,
			},
		};

		target.Run();

		Assert.True(assemblyFile.Exists());
	}
}
