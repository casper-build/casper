﻿using System;
using System.Runtime.InteropServices;
using Casper.IO;

namespace Casper;

[TestFixture]
[Timeout(10000)]
public class ExecTests : IDisposable {
	readonly IFileSystem fileSystem = RealFileSystem.Instance;
	readonly IDirectory workingDirectory;

	public ExecTests() {
		workingDirectory = fileSystem.MakeTemporaryDirectory();
	}

	public void Dispose() {
		workingDirectory.Delete();
	}

	[Test]
	public void ExecAndArguments() {
		var fooFile = workingDirectory.File("foo.txt");
		var barFile = workingDirectory.File("bar.txt");

		fooFile.WriteAllText("Hello World!");
		barFile.Delete();

		var target = new Exec {
			WorkingDirPath = workingDirectory.FullPath,
			TargetFilePath = MoveCommand,
			Arguments = "foo.txt bar.txt",
		};
		target.Run();

		Assert.False(fooFile.Exists());
		Assert.True(barFile.Exists());
		Assert.That(barFile.ReadAllText(), Is.EqualTo("Hello World!"));
	}

	[Test]
	public void Fail() {
		var fooFile = workingDirectory.File("foo.txt");
		var barFile = workingDirectory.File("bar.txt");
		fooFile.Delete();
		barFile.Delete();

		var target = new Exec {
			WorkingDirPath = workingDirectory.FullPath,
			TargetFilePath = MoveCommand,
			Arguments = "foo.txt bar.txt",
		};

		Assert.Throws<CasperException>(() => target.Run());
		Assert.False(fooFile.Exists());
		Assert.False(barFile.Exists());

		Assert.That(target.Result!.StandardError, Is.Not.Null.And.Not.Empty);
	}

	[Test]
	public void MissingExecutable() {
		var target = new Exec {
			WorkingDirPath = workingDirectory.FullPath,
			Arguments = "foo.txt bar.txt",
		};

		Assert.Throws<CasperException>(() => target.Run());
	}

	string MoveCommand => RuntimeInformation.IsOSPlatform(OSPlatform.Windows) ? "move" : "mv";
}
