global using NUnit.Framework;

[assembly: FixtureLifeCycle(LifeCycle.InstancePerTestCase)]
[assembly: Parallelizable(ParallelScope.Children)]
[assembly: Timeout(100)] // Global timeout; individual tests may increase this timeout
