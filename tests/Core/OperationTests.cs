using System;

namespace Casper; 

public class OperationTests {
	[Test]
	public void Failure() {
		var target = new CustomOperation(() => throw new Exception("Fail!"));

		var ex = Assert.Throws<CasperException>(() => target.Run())!;
		Assert.Multiple(() => {
			Assert.That(ex.InnerException, Is.TypeOf<Exception>());
			Assert.That(ex.InnerException!.Message, Is.EqualTo("Fail!"));
		});

		Assert.IsTrue(target.Finished);
	}
}
