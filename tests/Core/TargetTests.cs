﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Casper; 

public class TargetTests {

	[Test]
	public void RunTarget() {
		var sentinel = false;

		var target = new CustomTarget(() => sentinel = true);

		target.Run();

		Assert.IsTrue(target.Finished);
		Assert.IsTrue(sentinel);
	}

	[Test]
	public void RunDependentTargetsFirst() {
		var executed = new List<string>();

		var targetLast = new CustomTarget(() => executed.Add("targetLast"));
		var targetFirst = new CustomTarget(() => executed.Add("targetFirst"));
		targetLast.DependsOn(targetFirst);

		targetLast.Run();

		Assert.IsTrue(targetFirst.Finished);
		Assert.IsTrue(targetLast.Finished);
		Assert.That(executed, Is.EquivalentTo(new[] { "targetFirst", "targetLast" }));
	}

	class TestGate {
		private static readonly TimeSpan GateTimeout = TimeSpan.FromSeconds(1);
		private readonly ManualResetEvent gate = new(false);

		public void Unlock() => Assert.IsTrue(gate.Set());
		public void Wait() => Assert.IsTrue(gate.WaitOne(GateTimeout));
	}

	class TestGatePair {
		private readonly TestGate gateA = new();
		private readonly TestGate gateB = new();

		public void ArriveA() {
			gateA.Unlock();
			gateB.Wait();
		}

		public void ArriveB() {
			gateB.Unlock();
			gateA.Wait();
		}
	}

	[Test]
	public void RunNonDependentTasksInParallel() {
		var gates = new TestGatePair();
		var targetList = new LifecycleTarget();

		var targetA = new CustomTarget(() => { gates.ArriveA(); });
		var targetB = new CustomTarget(() => { gates.ArriveB(); });
		var targetC = new LifecycleTarget();
		targetA.DependsOn(targetC);
		targetB.DependsOn(targetC);
		targetList.DependsOn(targetA);
		targetList.DependsOn(targetB);

		targetList.Run();

		Assert.IsTrue(targetA.Finished);
		Assert.IsTrue(targetB.Finished);
	}

	[Test]
	public void Failure() {
		var target = new CustomTarget(() => throw new Exception("Fail!"));

		var ex = Assert.Throws<CasperException>(() => target.Run())!;
		Assert.That(ex.InnerException, Is.TypeOf<CasperException>());
		Assert.Multiple(() => {
			Assert.That(ex.InnerException!.InnerException, Is.TypeOf<Exception>());
			Assert.That(ex.InnerException.InnerException!.Message, Is.EqualTo("Fail!"));
		});

		Assert.IsTrue(target.Finished);
	}

	[Test]
	public void DisallowCircularDependencies() {
		// Checking for circular dependencies at the time a dependency is added to aid debugging.
		// Since targets don't have names yet, there is no good way to indicate the tasks in the circle if checking is
		// delayed until target execution.
		// However, checking every time is inefficient, and could impact performance in a large graph.
		// Once targets have names, circular dependency checking should be moved to execution time
		var targetLast = new LifecycleTarget();
		var targetNext = new LifecycleTarget();
		targetLast.DependsOn(targetNext);
		Assert.Throws<CasperException>(() => targetNext.DependsOn(targetLast));
		Assert.IsFalse(targetNext.TransitivelyDependsOn(targetLast));
		var targetFirst = new LifecycleTarget();
		targetNext.DependsOn(targetFirst);
		Assert.Throws<CasperException>(() => targetFirst.DependsOn(targetNext));
		Assert.IsFalse(targetFirst.TransitivelyDependsOn(targetNext));
		Assert.Throws<CasperException>(() => targetFirst.DependsOn(targetLast));
		Assert.IsFalse(targetFirst.TransitivelyDependsOn(targetLast));
	}

	[Test]
	public void DeferDefault() {
		var target = new Target<TestOperation>();
		Assert.False(TestOperation.Created());
		target.Run();
		Assert.True(target.Finished);
		Assert.True(TestOperation.Created());
	}

	[Test]
	public void DeferInit() {
		var target = new Target<TestOperation>(_ => _.Init(() => new()));
		Assert.False(TestOperation.Created());
		target.Run();
		Assert.True(target.Finished);
		Assert.True(TestOperation.Created());
	}

	private class TestOperation : Operation {
		private static readonly HashSet<string> createdBy = new();

		public static bool Created() {
			var testName = TestContext.CurrentContext.Test.Name;
			return createdBy.Contains(testName);
		}

		public TestOperation() {
			Assert.True(createdBy.Add(TestContext.CurrentContext.Test.Name), $"{nameof(TestOperation)} was created more than once");
		}

		protected override void RunThis() {
		}
	}
}
