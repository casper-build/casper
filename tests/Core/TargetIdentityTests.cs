using Casper.IO;

namespace Casper;

public class TargetIdentityTests {
	[Test]
	[Ignore("This doesn't work with the current pattern, but it needs to")]
	public void TargetPropertyIdentity() {
		var project = new TargetPropertyIdentityProject();
		Assert.Multiple(() => {
			Assert.AreSame(project.A, project.A);
			Assert.AreSame(project.B, project.B);
			Assert.AreNotSame(project.A, project.B);
		});
	}

	private class TargetPropertyIdentityProject : Project {
		public Target<CustomOperation> A => new(_ => { });
		public Target<CustomOperation> B => new(_ => { });
		public TargetPropertyIdentityProject() : base(RealFileSystem.Instance.GetCurrentDirectory()) { }
	}
}
