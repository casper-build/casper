﻿namespace Casper.IO; 

[TestFixture]
[NonParallelizable]
public class RealDirectoryTests : DirectoryTests {
	protected override IFileSystem GetFileSystemInstance() {
		return RealFileSystem.Instance;
	}
}

[TestFixture]
[NonParallelizable]
public class RealFileTests : FileTests {
	protected override IFileSystem GetFileSystemInstance() {
		return RealFileSystem.Instance;
	}
}
