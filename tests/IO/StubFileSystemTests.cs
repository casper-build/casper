﻿namespace Casper.IO; 

[TestFixture]
public class StubDirectoryTests : DirectoryTests {
	protected override IFileSystem GetFileSystemInstance() {
		return new StubFileSystem();
	}
}

[TestFixture]
public class StubFileTests : FileTests {
	protected override IFileSystem GetFileSystemInstance() {
		return new StubFileSystem();
	}
}
