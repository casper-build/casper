﻿using System.Reflection;
using static System.IO.Path;

namespace Casper.IO;

[FixtureLifeCycle(LifeCycle.SingleInstance)]
[Parallelizable(ParallelScope.Fixtures)]
public abstract class DirectoryTests {

	private IFileSystem fileSystem = null!;
	private IDirectory testDirectory = null!;
	private IDirectory originalWorkingDirectory = null!;

	[OneTimeSetUp]
	public void SetUpOnce() {
		fileSystem = GetFileSystemInstance();
		originalWorkingDirectory = fileSystem.GetCurrentDirectory();

		var testParentDirectory = fileSystem.File(Assembly.GetExecutingAssembly().Location).Parent;
		testDirectory = testParentDirectory.Directory(this.GetType().Name);
		testDirectory.Delete();
		testDirectory.Create();
	}

	[SetUp]
	public void SetUp() {
		testDirectory.SetAsCurrent();
	}

	[OneTimeTearDown]
	public void TearDownOnce() {
		originalWorkingDirectory.SetAsCurrent();
		testDirectory.Delete();
	}

	protected abstract IFileSystem GetFileSystemInstance();

	[Test]
	public void RootDirectory() {
		if(System.IO.Path.DirectorySeparatorChar == '/') {
			Assert.That(testDirectory.RootDirectory.FullPath, Is.EqualTo("/"));
		} else {
			Assert.Inconclusive("Can't predetermine root path on Windows");
		}
	}

	[Test]
	public void RelativeDirectory() {
		const string directoryName = nameof(RelativeDirectory);
		var directory = fileSystem.Directory(directoryName);
		Assert.That(directory.Name, Is.EqualTo(directoryName));
		Assert.That(directory.FullPath, Is.EqualTo(Combine(testDirectory.FullPath, directoryName)));
	}

	[Test]
	public void AbsoluteDirectory() {
		var rootPath = testDirectory.RootDirectory.FullPath;
		const string directoryName = nameof(AbsoluteDirectory);
		var directoryPath = Combine(rootPath, directoryName);
		var directory = fileSystem.Directory(directoryPath);
		Assert.That(directory.Name, Is.EqualTo(directoryName));
		Assert.That(directory.FullPath, Is.EqualTo(Combine(rootPath, directoryPath)));
	}

	[Test]
	public void Files() {
		const string directoryName = nameof(Files);
		var directory = fileSystem.Directory(directoryName);
		directory.Delete();
		directory.Create();

		var fileA = directory.File("fileA.tmp");
		fileA.WriteAllText("");
		var fileB = directory.File("fileB.log.tmp");
		fileB.WriteAllText("");
		var fileC = directory.File("fileC.tmp.log");
		fileC.WriteAllText("");
		var dirD = directory.Directory("dirD.tmp.log");
		dirD.Create();

		Assert.That(directory.Files("fileA.tmp"), Is.EquivalentTo(new[] { fileA }));
		Assert.That(directory.Files("file*"), Is.EquivalentTo(new[] { fileA, fileB, fileC }));
		Assert.That(directory.Files("*.tmp"), Is.EquivalentTo(new[] { fileA, fileB }));
		Assert.That(directory.Files("*.log"), Is.EquivalentTo(new[] { fileC }));
		Assert.That(directory.Files("file*.log*"), Is.EquivalentTo(new[] { fileB, fileC }));

		directory.Delete();
	}

	[Test]
	public void Directories() {
		const string directoryName = nameof(Directories);
		var directory = fileSystem.Directory(directoryName);
		directory.Delete();
		directory.Create();

		var dirA = directory.Directory("dirA.tmp");
		dirA.Create();
		var dirB = directory.Directory("dirB.log.tmp");
		dirB.Create();
		var dirC = directory.Directory("dirC.tmp.log");
		dirC.Create();
		var fileD = directory.File("fileD.tmp.log");
		fileD.WriteAllText("fileD");

		Assert.That(directory.Directories("dirA.tmp"), Is.EquivalentTo(new[] { dirA }));
		Assert.That(directory.Directories("dir*"), Is.EquivalentTo(new[] { dirA, dirB, dirC }));
		Assert.That(directory.Directories("*.tmp"), Is.EquivalentTo(new[] { dirA, dirB }));
		Assert.That(directory.Directories("*.log"), Is.EquivalentTo(new[] { dirC }));
		Assert.That(directory.Directories("dir*.log*"), Is.EquivalentTo(new[] { dirB, dirC }));

		directory.Delete();
	}

	[Test]
	public void CreateDelete() {
		var testDirectory = this.testDirectory.Directory(nameof(CreateDelete));
		testDirectory.Delete();

		testDirectory.Delete();
		Assert.False(testDirectory.Exists());

		testDirectory.Create();
		Assert.True(testDirectory.Exists());

		testDirectory.Delete();
		Assert.False(testDirectory.Exists());
	}

	[Test]
	public new void ToString() {
		Assert.That(testDirectory.ToString(), Is.EqualTo($"<{testDirectory.FullPath}>"));
	}
}
