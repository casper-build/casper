﻿using System.Reflection;
using static System.IO.Path;

namespace Casper.IO;

[FixtureLifeCycle(LifeCycle.SingleInstance)]
public abstract class FileTests {

	private IFileSystem fileSystem = null!;
	private IDirectory testDirectory = null!;
	private IDirectory originalWorkingDirectory = null!;

	[OneTimeSetUp]
	public void SetUpOnce() {
		fileSystem = GetFileSystemInstance();
		originalWorkingDirectory = fileSystem.GetCurrentDirectory();

		var testParentDirectory = fileSystem.File(Assembly.GetExecutingAssembly().Location).Parent;
		testDirectory = testParentDirectory.Directory(this.GetType().Name);
		testDirectory.Delete();
		testDirectory.Create();
	}

	[SetUp]
	public void SetUp() {
		testDirectory.SetAsCurrent();
	}

	[OneTimeTearDown]
	public void TearDownOnce() {
		originalWorkingDirectory.SetAsCurrent();
		testDirectory.Delete();
	}

	protected abstract IFileSystem GetFileSystemInstance();

	[Test]
	public void RelativeFile() {
		var fileName = $"${nameof(AbsoluteFile)}.file";
		var file = fileSystem.File(fileName);
		Assert.That(file.Name, Is.EqualTo(fileName));
		Assert.That(file.FullPath, Is.EqualTo(Combine(testDirectory.FullPath, fileName)));
		Assert.That(file.Parent.FullPath, Is.EqualTo(Combine(testDirectory.FullPath)));
	}

	[Test]
	public void AbsoluteFile() {
		var rootPath = testDirectory.RootDirectory.FullPath;
		var fileName = $"${nameof(AbsoluteFile)}.file";
		var filePath = Combine(rootPath, fileName);
		var file = fileSystem.File(filePath);
		Assert.That(file.Name, Is.EqualTo(fileName));
		Assert.That(file.FullPath, Is.EqualTo(Combine(rootPath, filePath)));
		Assert.That(file.Parent.FullPath, Is.EqualTo(rootPath));
	}

	[Test]
	public void CreateDelete() {
		var testFile = fileSystem.File($"{nameof(CreateDelete)}.test");

		Assert.That(testFile.Exists, Is.False);

		testFile.WriteAllText("foo");

		Assert.That(testFile.Exists, Is.True);

		testFile.Delete();

		Assert.That(testFile.Exists, Is.False);
	}

	[Test]
	public new void ToString() {
		var testFile = fileSystem.File($"{nameof(ToString)}.test");
		Assert.That(testFile.ToString(), Is.EqualTo($"<{testFile.FullPath}>"));
	}
}
