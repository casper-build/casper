﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using Microsoft.CodeAnalysis;

namespace Generator;

[Generator]
public class ProjectGenerator : IIncrementalGenerator {
	private const string DefaultProjectFileName = "Casper.project.cs";
	private static char[] directorySeparators = new[] { Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar };

	public void Initialize(IncrementalGeneratorInitializationContext context) {
		var projectFiles = context.CompilationProvider.SelectMany((p, _) => p.SyntaxTrees)
			.Select((t, _) => t.FilePath).Where(p => p.EndsWith(DefaultProjectFileName)).Collect();
		context.RegisterSourceOutput(projectFiles, (output, allProjectFiles) => output.AddSource("CasperProjects", MakeProject(allProjectFiles)));
	}

	private string MakeProject(ImmutableArray<string> allProjectFiles) {
		var allProjectDirectories = allProjectFiles.Select(f => Path.GetDirectoryName(f)).ToArray();
		var commonPrefix = GetLongestCommonPrefix(allProjectDirectories);
		var commonPath = commonPrefix.Substring(0,
			commonPrefix.LastIndexOfAny(directorySeparators));
		var relativeProjectDirectories = allProjectDirectories.Select(projectDirectory => projectDirectory.Replace(commonPath, "").TrimStart(directorySeparators));
		var rootProject = new ProjectPath("");
		foreach (var relativeProjectDirectory in relativeProjectDirectories) {
			rootProject.Add(relativeProjectDirectory);
		}
		return MakeRootProject(commonPath, rootProject);
	}

	private string MakeRootProject(string commonPath, ProjectPath rootProject) {
		return $$"""
				{{MakeProjectClass(null, rootProject)}}
				public static partial class Projects {
					public static Casper.IO.IFileSystem FileSystem = Casper.IO.RealFileSystem.Instance;
					public static Casper.IO.IDirectory RootDirectory = Projects.FileSystem.Directory("{{commonPath}}");
					public static {{rootProject.Name}}.Project Root { get; } = new {{rootProject.Name}}.Project(Projects.RootDirectory, null);
				}
				""";
	}

	private string MakeChildProjects(ProjectPath project, Func<ProjectPath, String> generator) {
		return string.Join(Environment.NewLine, project.Values.Select(generator));
	}

	private string MakeProjectClass(string? namespacePrefix, ProjectPath project) {
		namespacePrefix = null == namespacePrefix ? project.Name : string.Join(".", namespacePrefix, project.Name);
		return $$"""
				namespace {{namespacePrefix}} {
					public partial class Project : Casper.Project {
						public Project(Casper.IO.IDirectory location, Casper.IO.IFile? projectFile) : base(location, projectFile) {
							{{MakeChildProjects(project, MakeProjectInit)}}
						}
						{{MakeChildProjects(project, MakeProjectProperty)}}
					}
				}
				{{MakeChildProjects(project, path => MakeProjectClass(namespacePrefix, path))}}
				""";
	}

	private string MakeProjectProperty(ProjectPath project) {
		return $$"""
public {{project.Name}}.Project {{project.Name}} { get; }
""";
	}

	private string MakeProjectInit(ProjectPath project) {
		return $$"""
{{project.Name}} = new {{project.Name}}.Project(location.Directory("{{project.Path}}"), location.Directory("{{project.Path}}").File("{{DefaultProjectFileName}}"));
""";
	}

	public static string GetLongestCommonPrefix(IReadOnlyList<string> s) {
		int k = s[0].Length;
		for (int i = 1; i < s.Count; i++) {
			k = Math.Min(k, s[i].Length);
			for (int j = 0; j < k; j++)
				if (s[i][j] != s[0][j]) {
					k = j;
					break;
				}
		}

		return s[0].Substring(0, k);
	}

	private class ProjectPath : ConcurrentDictionary<string, ProjectPath> {
		public string Path { get; }
		public string Name => Path == string.Empty ? "Root" : SafeName(Path);

		public ProjectPath(string path) {
			Path = path;
		}
			
		public void Add(string path) {
			try {
				var parts = path.Split(directorySeparators);
				var enumerator = parts.Cast<string>().GetEnumerator();
				enumerator.MoveNext();
				Add(enumerator);
			} catch (Exception ex) {
				throw new Exception($"Failed to add {path}: {ex.Message}", ex);
			}
		}
			
		public void Add(IEnumerator<string> parts) {
			var child = GetOrAdd(parts.Current, x => new ProjectPath(x));
			if (parts.MoveNext()) {
				child.Add(parts);
			} else {
				child.IsProject = true;
			}
		}

		private static string SafeName(string path) =>
			// TODO: do this for all illegal characters
			path.Replace('.', '_');

		public bool IsProject { get; private set; }
	}
}
