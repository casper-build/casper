using System;
using CliWrap;
using CliWrap.Buffered;
using CliWrap.Exceptions;

namespace Casper; 

public class Exec : Operation {

	public string TargetFilePath { get; init; } = string.Empty;
	public string Arguments { get; init; } = string.Empty;
	public string WorkingDirPath { get; init; } = Environment.CurrentDirectory; // TODO: this should be the project dir

	protected override void RunThis() {
		var command = new Command(TargetFilePath)
			.WithArguments(Arguments)
			.WithWorkingDirectory(WorkingDirPath)
			.WithValidation(CommandResultValidation.None);
		Result = command
			.ExecuteBufferedAsync().GetAwaiter().GetResult()
		;
		if (Result.ExitCode != 0) {
			throw new CommandExecutionException(
				command,
				Result.ExitCode,
				$@"
Command execution failed because the underlying process returned a non-zero exit code ({Result.ExitCode}).

Command:
{TargetFilePath} {Arguments}

Standard output:
{Result.StandardOutput.Trim()}

Standard error:
{Result.StandardError.Trim()}
				".Trim()
				);
		}
	}

	// TODO: remove nullable, move this to target?
	public BufferedCommandResult? Result { get; private set; }
}
