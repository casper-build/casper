﻿using Casper.IO;

namespace Casper;

public class Project {
	public IDirectory Directory { get; }
	public IDirectory OutputDirectory => Directory.Directory("output");
	public IFile? ProjectFile { get; }

	public Project(IDirectory directory, IFile? projectFile = null) {
		Directory = directory;
		ProjectFile = projectFile;
	}
}
