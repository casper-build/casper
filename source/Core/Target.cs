﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RunContext = System.Collections.Generic.Dictionary<Casper.Target, System.Threading.Tasks.Task>;

namespace Casper;

public abstract class Target {
	private readonly List<Target> dependencies = new();
	public bool Finished { get; private set; }

	private IEnumerable<Task> RunDependencies(RunContext runContext) {
		return dependencies.Select(dep => dep.RunInContext(runContext));
	}

	public void DependsOn(Target dependency) {
		if (dependency.TransitivelyDependsOn(this)) {
			throw new CasperException("Circular dependency detected");
		}

		dependencies.Add(dependency);
	}

	public bool TransitivelyDependsOn(Target dependency) {
		return dependencies.Contains(dependency) || dependencies.Any(dep => dep.TransitivelyDependsOn(dependency));
	}

	public void Run() {
		ConfigureThis(); // TODO: configure all tasks in graph explicitly upfront to aid troubleshooting
		var runContext = new RunContext();
		try {
			RunInContext(runContext).Wait();
		}
		catch (AggregateException ex) {
			if (ex.InnerExceptions.Count == 1) {
				throw new CasperException("Target failed", ex.InnerExceptions[0]);
			}
		}
		// TODO: wrap other exceptions also?
	}

	private Task RunInContext(RunContext runContext) {
		var dependenciesTasks = RunDependencies(runContext).ToArray();
		if (!runContext.TryGetValue(this, out var runThisTask)) {
			runThisTask = Task.Run(async () => {
				try {
					await Task.WhenAll(dependenciesTasks);
					RunThis();
				}
				finally {
					Finished = true;
				}
			});
			runContext.Add(this, runThisTask);
		}
		return runThisTask;
	}

	protected abstract void ConfigureThis();
	protected abstract void RunThis();
}

public class Target<T> : Target where T : Operation {
	private readonly Lazy<T> target;

	public Target() {
		target = new Lazy<T>(Activator.CreateInstance<T>);
	}

	public Target(Action<ITargetBuilder<T>> builder) {
		target = new Lazy<T>(() => {
			var targetBuilder = new TargetBuilder<T>(this);
			builder(targetBuilder);
			return targetBuilder.Build();
		});
	}

	public T Operation => target.Value;

	protected override void ConfigureThis() => GC.KeepAlive(Operation);

	protected override void RunThis() => Operation.Run();
}

public class CustomTarget : Target<CustomOperation> {
	public CustomTarget(Action action)
		: base(_ => _.Init(() => new CustomOperation(action))) {
	}
}

// Target designed to conveniently run other targets, but not do any work itself 
public class LifecycleTarget : Target {
	protected override void ConfigureThis() { /* Intentionally empty */ }
	protected override void RunThis() { /* Intentionally empty */ }
}

public interface ITargetBuilder<T> {
	ITargetBuilder<T> Init(Func<T> init);
	ITargetBuilder<T> DependsOn(Target dependency);
}

class TargetBuilder<T> : ITargetBuilder<T> where T : Operation {
	private readonly Target<T> target;
	private Func<T> init = Activator.CreateInstance<T>;

	public TargetBuilder(Target<T> target) {
		this.target = target;
	}

	public ITargetBuilder<T> Init(Func<T> init) {
		this.init = init;
		return this;
	}

	public ITargetBuilder<T> DependsOn(Target dependency) {
		target.DependsOn(dependency);
		return this;
	}

	public T Build() {
		return init();
	}
}
