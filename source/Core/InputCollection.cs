using System.Collections;
using System.Collections.Generic;

namespace Casper;

// Collection type designed to represent collections of inputs in the DSL
public class InputCollection<T> : IEnumerable<T> {
	private readonly List<T> collection = new ();

	public IEnumerator<T> GetEnumerator() {
		return collection.GetEnumerator();
	}

	IEnumerator IEnumerable.GetEnumerator() {
		return ((IEnumerable)collection).GetEnumerator();
	}

	public void Add(T element) {
		collection.Add(element);
	}

	public void Add(IEnumerable<T> elements) {
		collection.AddRange(elements);
	}
}
