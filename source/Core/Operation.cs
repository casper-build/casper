using System;

namespace Casper;

public abstract class Operation {
	public bool Finished { get; private set; }
	
	public void Run() {
		try {
			RunThis();
		}
		catch (Exception ex) {
			throw new CasperException("Operation failed", ex);
		}
		finally {
			Finished = true;
		}
	}

	protected abstract void RunThis();
}

public class CustomOperation : Operation {
	private readonly Action action;

	public CustomOperation(Action action) {
		this.action = action;
	}

	protected override void RunThis() {
		action();
	}
}
