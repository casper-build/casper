﻿using System;

namespace Casper; 

public class CasperException : Exception {
	public CasperException(string message) : base(message) { }
	public CasperException(string message, Exception inner) : base(message, inner) { }
}
