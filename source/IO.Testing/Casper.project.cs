﻿using System.Linq;
using Casper;
using Microsoft.CodeAnalysis;

namespace Root.IO_Testing;

partial class Project {
	public Target<CompileCSharp> Compile => new(_ => _
		.Init(() => new() {
			Input = {
				TargetFramework = TargetFrameworks.Net60,
				References = { Projects.Root.IO.Compile },
				SourceFiles = { Directory.Files("*.cs").Where(f => !f.Equals(ProjectFile)) },
			},
			Output = {
				AssemblyName = "Casper.IO.Testing",
				Kind = OutputKind.DynamicallyLinkedLibrary,
				Directory = OutputDirectory.FullPath,
			},
			Nullable = NullableContextOptions.Enable,
		})
		.DependsOn(Projects.Root.IO.Compile)
	);
}
