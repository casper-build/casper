namespace Casper.IO;

public interface IFileSystemObject {
	string Name { get; }
	string FullPath { get; }
	IDirectory Parent { get; }

	bool Exists();
	void Delete();
}
