﻿using System.Linq;
using Casper;
using Microsoft.CodeAnalysis;

namespace Root.IO;

partial class Project {
	public Target<CompileCSharp> Compile => new(_ => _
		.Init(() => new() {
			Input = {
				TargetFramework = TargetFrameworks.Net60,
				SourceFiles = { Directory.Files("*.cs").Where(f => !f.Equals(ProjectFile)) },
			},
			Output = {
				AssemblyName = "Casper.IO",
				Directory = OutputDirectory.FullPath,
				Kind = OutputKind.DynamicallyLinkedLibrary,
			},
			Nullable = NullableContextOptions.Enable,
		}));
}
