using System;

namespace Casper;

public static class ScopeFunctions {
	// Helps with calling a method with a non-nullable parameter on a nullable argument, e.g.
	// string? maybeNull = null;
	// maybeNull?.Let(it => new Uri(maybeNull))
	public static R Let<T, R>(this T source, Func<T, R> transform) => transform(source);
}
