﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Casper.IO; 

public class RealFileSystem : IFileSystem {

	public static readonly RealFileSystem Instance = new();

	private RealFileSystem() { }
		
	public IFile File(string path) {
		return new RealFile(path);
	}

	public IDirectory Directory(string path) {
		return new RealDirectory(path);
	}

	public IDirectory GetCurrentDirectory() {
		return Directory(System.IO.Directory.GetCurrentDirectory());
	}

	public IDirectory MakeTemporaryDirectory() {
		var tempFolder = Path.GetTempFileName();
		System.IO.File.Delete(tempFolder);
		var result = Directory(tempFolder);
		result.Create();

		return result;
	}

	private class RealFile : IFile {
		public RealFile(string path) {
			FullPath = System.IO.Path.IsPathRooted(path) 
				? path 
				: System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), path);
		}

		public void WriteAllText(string text) {
			System.IO.File.WriteAllText(FullPath, text);
		}

		public string ReadAllText() {
			return System.IO.File.ReadAllText(FullPath);
		}

		public IEnumerable<string> ReadAllLines() {
			return System.IO.File.ReadAllLines(FullPath);
		}

		public bool Exists() {
			return System.IO.File.Exists(FullPath);
		}

		public void Delete() {
			System.IO.File.Delete(FullPath);
		}

		public void CopyTo(IFile destination) {
			System.IO.File.Copy(FullPath, destination.FullPath, true);
		}

		public void CreateDirectories() {
			System.IO.Directory.CreateDirectory(DirectoryPath);
		}

		public TextReader OpenText() {
			return System.IO.File.OpenText(FullPath);
		}

		public DateTimeOffset LastWriteTimeUtc => System.IO.File.GetLastWriteTimeUtc(FullPath);

		public string FullPath { get; }

		private string DirectoryPath => System.IO.Path.GetDirectoryName(FullPath)!;

		public IDirectory Parent => new RealDirectory(DirectoryPath);

		public string Name => System.IO.Path.GetFileName(FullPath);

		public override string ToString() => $"<{FullPath}>";

		#region Equality

		public bool Equals(IFile? other) {
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return FullPath == other.FullPath;
		}

		public override bool Equals(object? obj) {
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((IFile)obj);
		}

		public override int GetHashCode() {
			return FullPath.GetHashCode();
		}

		#endregion
	}

	private class RealDirectory : IDirectory {
		public RealDirectory(string path) {
			FullPath = System.IO.Path.IsPathRooted(path) 
				? path 
				: System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), path);
		}

		public IFile File(string relativePath) {
			return new RealFile(System.IO.Path.Combine(FullPath, relativePath));
		}

		public IEnumerable<IFile> Files(string pattern) {
			// Might need to expose EnumerationOptions (with a better name)
			return System.IO.Directory.EnumerateFiles(FullPath, pattern).Select(f => new RealFile(f));
		}

		public IDirectory Directory (string relativePath) {
			return new RealDirectory(System.IO.Path.Combine(FullPath, relativePath));
		}

		public IEnumerable<IDirectory> Directories(string pattern) {
			// Might need to expose EnumerationOptions (with a better name)
			return System.IO.Directory.EnumerateDirectories(FullPath, pattern).Select(f => new RealDirectory(f));
		}

		public void SetAsCurrent() {
			System.IO.Directory.SetCurrentDirectory(FullPath);
		}

		public bool Exists() {
			return System.IO.Directory.Exists(FullPath);
		}

		public void Delete() {
			try {
				System.IO.Directory.Delete(FullPath, true);
			} catch(DirectoryNotFoundException) {
				// Desired result is for directory to not exist, which is true
				// Consider this successful
			}
		}

		public void Create() {
			System.IO.Directory.CreateDirectory(FullPath);
		}

		public string FullPath { get; }

		private string DirectoryPath => System.IO.Path.GetDirectoryName(FullPath)!;

		public IDirectory Parent => new RealDirectory(DirectoryPath);

		public IDirectory RootDirectory => Directory(System.IO.Directory.GetDirectoryRoot(FullPath));

		public string Name => System.IO.Path.GetFileName(FullPath);

		public override string ToString() => $"<{FullPath}>";

		#region Equality

		public bool Equals(IDirectory? other) {
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return FullPath == other.FullPath;
		}

		public override bool Equals(object? obj) {
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((IFile)obj);
		}

		public override int GetHashCode() {
			return FullPath.GetHashCode();
		}

		#endregion
	}
}
