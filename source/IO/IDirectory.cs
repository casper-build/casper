using System;
using System.Collections.Generic;

namespace Casper.IO;

public interface IDirectory : IFileSystemObject, IEquatable<IDirectory> {
	IFile File(string relativePath);
	IDirectory Directory(string relativePath);
	void SetAsCurrent();
	void Create();

	IDirectory RootDirectory { get; }
	IEnumerable<IDirectory> Directories(string pattern);
	IEnumerable<IFile> Files(string pattern);
}
