using System;
using System.Collections.Generic;
using System.IO;

namespace Casper.IO; 

public interface IFile : IFileSystemObject, IEquatable<IFile> {
	void CopyTo(IFile destination);

	void WriteAllText(string text);

	TextReader OpenText();

	string ReadAllText();
	IEnumerable<string> ReadAllLines();

	void CreateDirectories();

	DateTimeOffset LastWriteTimeUtc { get; }
}
