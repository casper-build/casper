﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Casper.IO;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Emit;
using NuGet.Frameworks;

namespace Casper;

public class TargetFramework {
	public TargetFramework(IEnumerable<PortableExecutableReference> allReferences) {
		AllReferences = allReferences;
	}
	public IEnumerable<PortableExecutableReference> AllReferences { get; }
}

public static class TargetFrameworks {
	public static readonly TargetFramework Net60 = new(Basic.Reference.Assemblies.Net60.References.All);
}

public class CompileCSharp : Operation {
	public Inputs Input { get; } = new();
	public Outputs Output { get; } = new();
	public int WarningLevel { get; set; } = 6; // TODO: this value can change with new sdk versions
	public NullableContextOptions Nullable { get; set; } = NullableContextOptions.Disable;

	public record Inputs {
		public TargetFramework TargetFramework { get; set; } = TargetFrameworks.Net60;
		public InputCollection<IFile> SourceFiles { get; } = new();
		public InputCollection<CompileReference> References { get; } = new();
	}

	public record Outputs {
		public string AssemblyName { get; set; } = "";
		public string Directory { get; set; } = "";
		public OutputKind Kind { get; set; } = OutputKind.DynamicallyLinkedLibrary;

		public string AssemblyFileName => AssemblyName + ".dll";
		public string Location => Path.Combine(Directory, AssemblyFileName);
	}

	protected override void RunThis() {
		var compilation = CSharpCompilation.Create(Output.AssemblyName);
		compilation = compilation.WithOptions(new CSharpCompilationOptions(
			warningLevel: WarningLevel,
			nullableContextOptions: Nullable,
			outputKind: Output.Kind
		));
		compilation = compilation.AddSyntaxTrees(Input.SourceFiles.Select(file =>
			CSharpSyntaxTree.ParseText(file.ReadAllText(), encoding: Encoding.UTF8, path: file.FullPath)));
		compilation = compilation.AddReferences(Input.TargetFramework.AllReferences);
		compilation = compilation.AddReferences(Input.References.SelectMany(reference => reference.GetReferences(NuGetFramework.Parse("net6.0"))));

		Directory.CreateDirectory(Path.GetDirectoryName(Output.Location)!);
		using var assemblyStream = File.OpenWrite(Output.Location);
		using var pdbStream = File.OpenWrite(Path.ChangeExtension(Output.Location, "pdb"));
		var emitOptions = new EmitOptions(
			debugInformationFormat: DebugInformationFormat.PortablePdb,
			highEntropyVirtualAddressSpace: true
		);
		var result = compilation.Emit(assemblyStream, pdbStream, options: emitOptions);
		if (!result.Success) {
			var failureMessage = "Compilation failed:";
			foreach (var resultDiagnostic in result.Diagnostics) {
				failureMessage += $"{Environment.NewLine}{resultDiagnostic}";
			}
			throw new CasperException(failureMessage);
		}
	}
}
