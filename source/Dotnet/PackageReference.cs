using System;
using System.Collections.Generic;
using System.Linq;
using Casper.IO;
using Microsoft.CodeAnalysis;
using NuGet.Frameworks;

namespace Casper;

public class PackageReference : CompileReference {
	public string Name { get; }
	public string Version { get; }
	private readonly IDirectory packagesDirectory;
	private static readonly FrameworkReducer frameworkReducer = new();

	public PackageReference(string name, string version, IDirectory? packagesDirectory = null) {
		Name = name;
		Version = version;
		this.packagesDirectory = packagesDirectory ?? GetDefaultPackagesDirectory();
	}

	public override IEnumerable<MetadataReference> GetReferences(NuGetFramework targetFramework) {
		var chosenFramework = ChooseFramework(targetFramework);
		if (null == chosenFramework) {
			throw new Exception($"No compatible TFM found in package {Name}:{Version}");
		}
		return chosenFramework.Files("*.dll")
			.Select(f => MetadataReference.CreateFromFile(f.FullPath));
	}

	internal IDirectory? ChooseFramework(NuGetFramework targetFramework) {
		var availableFrameworks = GetAvailableFrameworks();
		var chosenFramework = frameworkReducer.GetNearest(targetFramework, availableFrameworks.Keys);
		return chosenFramework?.Let(it => availableFrameworks[it]);
	}

	private IDictionary<NuGetFramework, IDirectory> GetAvailableFrameworks() {
		var libDirectory = packagesDirectory
			.Directory(Name.ToLowerInvariant())
			.Directory(Version.ToLowerInvariant())
			.Directory("lib");
		var packageFrameworks = libDirectory
			.Directories("*")
			.ToDictionary(dir => NuGetFramework.ParseFolder(dir.Name), dir => dir);
		return packageFrameworks;
	}

	private static IDirectory GetDefaultPackagesDirectory() {
		return RealFileSystem.Instance
			.Directory(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile, Environment.SpecialFolderOption.DoNotVerify))
			.Directory(".nuget")
			.Directory("packages");
	}
}
