using System.Collections.Generic;
using Microsoft.CodeAnalysis;
using NuGet.Frameworks;

namespace Casper;

public abstract class CompileReference {

	private class AssemblyReference : CompileReference {
		private readonly string assemblyLocation;

		public AssemblyReference(string assemblyLocation) {
			this.assemblyLocation = assemblyLocation;
		}

		public override IEnumerable<MetadataReference> GetReferences(NuGetFramework targetFramework) {
			yield return MetadataReference.CreateFromFile(assemblyLocation);
		}
	}

	public abstract IEnumerable<MetadataReference> GetReferences(NuGetFramework targetFramework);

	public static implicit operator CompileReference(Target<CompileCSharp> target) =>
		new AssemblyReference(target.Operation.Output.Location);
}
